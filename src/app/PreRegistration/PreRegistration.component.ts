import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BieService } from '../Services/bie.service';

@Component({
  selector: 'app-PreRegistration',
  templateUrl: './PreRegistration.component.html',
  styleUrls: ['./PreRegistration.component.css']
})
export class PreRegistrationComponent implements OnInit {

  constructor( private bieService:BieService) { }

  formdata: FormGroup = new FormGroup({});

   ngOnInit() {
      this.formdata = new FormGroup({
         board: new FormControl(null,[Validators.required]),
         passedout:new FormControl(null,[Validators.required]),
         otherboard:new FormControl(null,[Validators.required]),
         hallticket:new FormControl("",[Validators.required]),
         dob:new FormControl(null,[Validators.required]),
         aadhaar:new FormControl("",[Validators.required]),
         mobile_no:new FormControl("",[Validators.required]),
         student_name:new FormControl("",[Validators.required]),
         gender:new FormControl("",[Validators.required]),
         father_name:new FormControl("",[Validators.required]),
         declaration:new FormControl("",[Validators.required]),

      });
   }
   SavePreData(){
    console.log( this.formdata)
   }

   OnChangeBoard(){
   this.bieService.getBieDetails()





   }
}
