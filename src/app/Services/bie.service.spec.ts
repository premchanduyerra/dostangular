/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { BieService } from './bie.service';

describe('Service: Bie', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BieService]
    });
  });

  it('should ...', inject([BieService], (service: BieService) => {
    expect(service).toBeTruthy();
  }));
});
