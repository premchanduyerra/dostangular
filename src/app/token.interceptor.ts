import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { AuthService } from 'src/app/services/Auth.service';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../environments/environment';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { DialogBoxComponent } from '../dialog/dialogBox/dialogBox.component';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService, public dialog: MatDialog, public router: Router) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const baseUrl = environment.baseUrl;
    request = request.clone({
      url: baseUrl + request.url
    });
    if (request.headers.get('skip')) {
      return next.handle(request);
    }
    request = request.clone({
      headers: new HttpHeaders({
        'token': `${this.auth.getToken()}`, 'userName': `${this.auth.getUserName()}`,
        'clientId': `${this.auth.getClientId()}`, 'Access-Control-Allow-Origin': '*'
      })
        .append('Access-Control-Allow-Origin', baseUrl)
    });
    return next.handle(request).pipe(map((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        console.log(event);
        if (event.body.status_Code === 999) {
          this.openDialogForError(event.body.status_Message);
        }
      }
      return event;
    }),
      catchError((error: HttpErrorResponse) => {
        let data = {};
        data = {
          reason: error && error.error && error.error.reason ? error.error.reason : '',
          status: error.status
        };
        // this.errorDialogService.openDialog(data);
        return throwError(error);
      }));
  }

  openDialogForError( obj:string) {
    console.log(obj);
    this.dialog.open(DialogBoxComponent, {
      width: '450px',
      data: { action: 'error', status: obj }
    }).afterClosed().subscribe(result => {
      this.router.navigateByUrl('login');
    });
  }

}
